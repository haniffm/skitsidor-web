---
title: "About"
draft: false
author: "Hanif FM"
---

Den här sidans mål är att höja kvaliten på svenska siter. Fokus är främst på statliga och siter med större antal användare.

Sidan är helt Free och OpenSource. Källan hittar ni [här](https://gitlab.com/haniffm/skitsidor-web).
