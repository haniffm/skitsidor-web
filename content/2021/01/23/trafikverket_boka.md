---
title: "Trafikverket.se/Boka är en Skitsida för att..."
date: 2021-01-23T23:28:08+01:00
draft: false
author: "Hanif FM"
---

1. Går ej att ändra höjden på kartan, medans det finns gott om plats längre ner på sidan.


![liten karta](/images/tfs_boka_01.webp)


2. En sökning tar ca 5-6 sekunder. Varav ca 1,6 - 2,5 sekunder är en API-anrop och resten sker på frontend.


![Långsam](/images/tfs_boka_02.webp)


3. Tider kan avbokas och då frigörs en lucka för en annan person. Det finns dock ingen möjlighet för en användare att få notifiering om detta. Detta öppnar upp för att de kunniga användarna skriver en liten skript som parsar sidan med jämna intervall och notifierar om det dyker upp tider i det sökta intervallet. Eller så har man inte den kunskapen och då väljer man att logga in på sidan eller ringa trafikverket med jämna mellanrum för att få boka tid.

4. När system-uppdateringar görs, tas hela tjänsten ner, och det verkar ta några timmar innan tjänsten är uppe igen. Utöver det får användaren ingen information om när tjänsten är uppe igen, utan man möts bara av en sådan här vy:

![Långsam](/images/tfs_systemet_uppdateras.webp)

## Lösningar

1. Öka höjden på kartan till det dubbla.

2. Hur kan en sökning ta så lång tid? Jag ser också att en del av beräkningen görs på frontenden (i webläsaren). T.ex. när man ändrar `Vill du hyra bil av Trafikverket?` från `Ja, manuell` till `Ja, automat`. Närmare titt visar att en 500+ KB json data skickas från backend till webläsaren. Datat innehåller massor med dubbletter som skulle kunna filtreras bort på backend och då går överföringshastigheten mycket fortare. Det gör också att frontenden har mycket mindre data och hantera och kan bli mycket snabbare.

Här är ett utdrag av JSON-datat:

```
{
  "data": [
    {
      "occasions": [
        {
          "examinationId": null,
          "examinationCategory": 2,
          "duration": {
            "start": "2021-06-21T14:45:00+02:00",
            "end": "2021-06-21T15:30:00+02:00"
          },
          "examinationTypeId": 12,
          "locationId": 1000019,
          "occasionChoiceId": 1,
          "vehicleTypeId": 4,
          "languageId": 13,
          "tachographTypeId": 1,
          "name": "Körprov B",
          "properties": null,
          "date": "2021-06-21",
          "time": "14:45",
          "locationName": "Farsta",
          "cost": "1.300 kr",
          "costText": " (med bil)",
          "increasedFee": false,
          "isEducatorBooking": null,
          "isLateCancellation": false,
          "isOutsideValidDuration": true,
          "isUsingTaxiKnowledgeValidDuration": false,
          "placeAddress": null,
          "placeCoordinate": null
        }
      ],
      "cost": "1.300 kr"
    },
    ...
  ]
}
```

Så här kan vi se hur många gånger varje fält upprepar sig (853 gånger):

```
cat tmp.json | jq .data[].occasions[].costText | uniq -c
    853 " (med bil)"

cat tmp.json | jq .data[].occasions[].locationName | uniq -c
    853 "Farsta"
```

De få fält som skiljer sig (och som användaren bryr sig om) är datum o tidpunkt:

```
duration
date
time
```

Tar man bort alla dessa dubbletter så sjunker storleken till ca 130KB.

3. Lägg till möjligheten att lägga in en intervall av datum och tider som passar användaren, så när de tiderna blir lediga så skickas ett mail/meddelande till personen och meddelar detta. Det gör att belastningen på bokningstjänsten (telefon och nätet) går ner avsevärt. Och folk kommer sluta skriva skript för att DDOS-a er.

4. Att tjänsten går ner totalt under en "system uppdatering" är helt oacceptabelt. Givet att det *faktiskt* är en "system uppdatering", så går det att lösa detta på lite olika sätt. Har man testa alla ändringar och litar på deploy-processen så borde detta inte ta mer än några minuter. Här är några tips:

- Automatisera deploy processen (givet att det inte redan är det).
- Skriv automatiserade tester för alla rörliga delar.
- Bryt ut front-end och backend så att de kan deployas oberoende av varandra.

Det finns mer som kan göras, men jag antar att ovanstående inte är uppfyllt och därav de långa ner-tiderna.

---


__Tills dess att ovan nämnda problem är lösta, så kommer fp.trafikverket.se/Boka att vara en Skitsite!__
