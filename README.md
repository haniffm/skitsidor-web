Skitsidor.se
---

En sida för att lista all skitsidor i Sverige med förslag på förbättringar.


Byggt på HUGO (statisk site generator). Och körs på netlify.

[![Netlify Status](https://api.netlify.com/api/v1/badges/81fe8041-6049-415c-962f-8793da86f571/deploy-status)](https://app.netlify.com/sites/skitsidor/deploys)

## Getting started

Prerequisites:
- Hugo (v0.92.2): https://gohugo.io

Run it:

```shell
hugo server
```

Then navigate to the printed URL (http://localhost:1313/).

